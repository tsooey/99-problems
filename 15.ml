let replicate ls n =
    let rec repeat x n acc = 
        if n = 0 then acc
        else repeat x (n-1) (x :: acc)
    in
    let rec aux ls n acc =
        match ls with
        | [] -> acc
        | x :: xs -> aux xs n (acc @ repeat x n [])
    in
    aux ls n []
