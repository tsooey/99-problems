let remove_at n ls = 
    let rec aux n acc = function
        | [] -> acc
        | x :: xs -> 
            if n = 0 then acc @ xs
            else aux (n-1) (acc @ [x]) xs
    in
    aux n [] ls
