let gray n =
    let rec aux k acc =
        if k >= n then acc
        else 
            let new_acc = List.map (fun x -> "0" ^ x) acc @
                          List.map (fun x -> "1" ^ x) (List.rev acc)
            in
            aux (k+1) new_acc
    in
    if n < 1 then []
    else aux 1 ["0"; "1"]
