let split ls n =
    let rec aux n acc = function
        | [] -> acc,[]
        | x :: xs -> 
            if n = 0 then acc, x :: xs
            else aux (n-1) (acc @ [x]) xs
    in
    aux n [] ls
