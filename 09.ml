let pack ls =
    let rec aux xs acc = function
        | [] -> acc
        | [x] -> acc @ [x :: xs]
        | x :: y :: ys -> 
            if x <> y then aux [] (acc @ [x :: xs]) (y :: ys)
            else aux (x :: xs) acc (y :: ys)
    in
    aux [] [] ls
