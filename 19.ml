let split ls n =
    let rec aux acc n = function
        | [] -> acc, []
        | x :: xs ->
            if n = 0 then acc, (x :: xs)
            else aux (acc @ [x]) (n-1) xs
    in
    aux [] n ls

let rotate ls n =
    let l = List.length ls
    in
    let m = ((n mod l) + l) mod l
    in
    let a, b = split ls m
    in
    b @ a
