type bool_expr =
    | Var of string
    | Not of bool_expr
    | And of bool_expr * bool_expr
    | Or of bool_expr * bool_expr

let rec eval var_vals expr =
    match expr with
    | Var s -> List.assoc s var_vals
    | Not e -> not (eval var_vals e)
    | And (e1, e2) -> (eval var_vals e1) && (eval var_vals e2)
    | Or (e1, e2) -> (eval var_vals e1) || (eval var_vals e2)

let table vars expr =
    let rec truth_table vars var_vals expr =
        match vars with
        | [] -> [(List.rev var_vals, eval var_vals expr)]
        | x :: xs ->
            truth_table xs ((x, true) :: var_vals) expr @ 
            truth_table xs ((x, false) :: var_vals) expr
    in
    truth_table vars [] expr
