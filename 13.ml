type 'a rle =
    | One of 'a
    | Many of int * 'a

let encode ls = 
    let rle ct x = 
        if ct = 0 then One x
        else Many (ct+1, x)
    in
    let rec aux ct acc = function
        | [] -> acc
        | [x] -> acc @ [rle ct x]
        | x :: y :: ys -> 
            if x <> y then aux 0 (acc @ [rle ct x]) (y :: ys)
            else aux (ct+1) acc (y :: ys)
    in
    aux 0 [] ls
