let slice ls s e =
    let rec aux i acc = function
        | [] -> acc
        | x :: xs ->
            if i >= s && i <= e then aux (i+1) (acc @ [x]) xs
            else aux (i+1) acc xs
    in
    aux 0 [] ls
