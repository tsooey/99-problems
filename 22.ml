let range s e =
    let rec aux s e acc =
        if s <= e then aux s (e-1) (e :: acc)
        else acc
    in
    if s <= e then aux s e [] else List.rev (aux e s [])
