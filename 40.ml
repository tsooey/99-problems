let goldbach n =
    let is_prime n =
        let rec is_prime_aux n d = 
            d * d > n || (n mod d <> 0 && is_prime_aux n (d+1))
        in
        n > 1 && is_prime_aux n 2
    in
    let rec aux n d =
        if d > n then None
        else
            if n mod 2 = 0 && is_prime d && is_prime (n-d) then Some (d, n-d)
            else aux n (d+1)
    in
    aux n 2
