let range s e =
    let rec aux s e acc =
        if s <= e then aux s (e-1) (e :: acc)
        else acc
    in
    if s <= e then aux s e [] else List.rev (aux e s [])

let rand_select ls n =
    let rec value_at ls n =
        match ls with
        | [] -> None
        | x :: xs -> 
            if n = 1 then Some x
            else value_at xs (n-1) 
    in
    let content_to_list opt =
        match opt with
        | None -> []
        | Some x -> [x]
    in
    let remove_at ls n =
        let rec remove_at_aux ls n acc = 
            match ls with
            | [] -> acc
            | x :: xs ->
                if n = 1 then acc @ xs
                else remove_at_aux xs (n-1) (acc @ [x])
        in
        remove_at_aux ls n []
    in
    let rec aux ls n l acc = 
        let r = (Random.int l) + 1
        in
        if n > 0 then aux (remove_at ls r) (n-1) (l-1) 
            (acc @ content_to_list (value_at ls r)) 
        else acc
    in
    aux ls n (List.length ls) [] 

let lotto_select n m = rand_select (range 1 m) n
