let length ls = 
    let rec aux acc = function
        | [] -> acc
        | x :: xs -> aux (acc+1) xs
    in
    aux 0 ls
