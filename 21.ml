let insert_at str i ls =
    let rec aux str i acc = function
        | [] -> acc @ [str]
        | x :: xs -> 
            if i = 0 then (acc @ str :: x :: xs)
            else aux str (i-1) (acc @ [x]) xs
    in
    aux str i [] ls
