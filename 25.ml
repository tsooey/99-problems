let permutation ls =
    let extract_index ls n = 
        let rec extract_index_aux ls n acc =
            match ls with
            | [] -> None, acc
            | x :: xs ->
                if n = 0 then Some x, (acc @ xs)
                else extract_index_aux xs (n-1) (acc @ [x])
        in
        extract_index_aux ls n [] 
    in
    let content_to_list opt =
        match opt with
        | None -> []
        | Some x -> [x]
    in
    let rec aux ls acc =
        let a, b = extract_index ls (Random.int (List.length ls))
        in
        if b = [] then (acc @ (content_to_list a))
        else aux b (acc @ (content_to_list a))
    in
    aux ls []
