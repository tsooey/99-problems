type 'a rle =
    | One of 'a
    | Many of int * 'a

let repeat x n =
    let rec aux acc x n =
        if n <= 0 then acc
        else aux (x :: acc) x (n-1)
    in 
    aux [] x n

let decode ls =
    let rec aux acc = function
        | [] -> acc
        | One x :: xs -> aux (acc @ [x]) xs
        | Many (n, x) :: xs -> aux (acc @ (repeat x n)) xs
    in
    aux [] ls
