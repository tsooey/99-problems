let duplicate ls = 
    let rec aux acc = function
        | [] -> acc
        | x :: xs -> aux (acc @ [x;x]) xs
    in
    aux [] ls
