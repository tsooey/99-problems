let encode ls =
    let rec aux ct acc = function
        | [] -> acc
        | [x] -> acc @ [(ct+1, x)]
        | x :: y :: ys -> 
            if x <> y then aux 0 (acc @ [(ct+1, x)]) (y :: ys)
            else aux (ct+1) acc (y :: ys)
    in
    aux 0 [] ls
