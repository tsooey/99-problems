let drop ls n =
    let rec aux acc i = function
        | [] -> acc
        | x :: xs -> 
            if i = n then aux acc 1 xs
            else aux (acc @ [x]) (i+1) xs
    in
    aux [] 1 ls
