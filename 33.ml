let coprime n m =
    let rec gcd n m =
        if m = 0 then n else gcd m (n mod m)
    in
    gcd n m = 1
