let rec at n = function
    | [] -> None
    | x :: xs -> if n = 1 then x else at (n-1) xs
