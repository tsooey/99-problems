type bool_expr =
    | Var of string
    | Not of bool_expr
    | And of bool_expr * bool_expr
    | Or of bool_expr * bool_expr

let rec eval a a_val b b_val expr =
    match expr with
    | Var s ->
        if s = a then a_val
        else 
            if s = b then b_val
            else failwith "invalid variable"
    | Not e -> not (eval a a_val b b_val e)
    | And (e1, e2) -> (eval a a_val b b_val e1) && (eval a a_val b b_val e2)
    | Or (e1, e2) -> (eval a a_val b b_val e1) || (eval a a_val b b_val e2)

let table a b expr =
    [(true, true, eval a true b true expr);
     (true, false, eval a true b false expr);
     (false, true, eval a false b true expr);
     (false, false, eval a false b false expr)]
