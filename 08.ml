let compress ls =
    let rec aux acc = function
        | []  -> acc
        | [x] -> acc @ [x]
        | x :: y :: ys -> 
            if x = y then aux acc (y :: ys)
            else aux (acc @ [x]) (y :: ys)
    in
    aux [] ls
