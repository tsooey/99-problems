let goldbach n =
    let is_prime n =
        let rec is_prime_aux n d =
            d * d > n || (n mod d <> 0 && is_prime_aux n (d+1))
        in
        n > 1 && is_prime_aux n 2
    in
    let rec aux n d =
        if d > n then None
        else
            if n mod 2 = 0 && is_prime d && is_prime (n-d) then Some (d, n-d)
            else aux n (d+1)
    in
    aux n 2

let goldbach_list n m =
    let rec aux n m acc =
        if n > m then acc
        else
            if m mod 2 = 0 then 
                match goldbach m with
                | Some x -> aux n (m-1) ((m, x) :: acc)
                | _ -> aux n (m-1) acc
            else aux n (m-1) acc
    in
    aux n m []

let goldbach_limit n m k =
    let rec aux n m k acc =
        if n > m then acc
        else
            if m mod 2 = 0 then
                match goldbach m with
                | Some (x, y) ->
                    if x > k && y > k then aux n (m-1) k ((m, (x, y)) :: acc)
                    else aux n (m-1) k acc
                | _ -> aux n (m-1) k acc
            else aux n (m-1) k acc
    in
    aux n m k []
