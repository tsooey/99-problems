type 'a rle =
    | One of 'a
    | Many of int * 'a

let encode ls =
    let rec aux ct acc = function
        | [] -> acc
        | [x] -> 
            if ct = 1 then acc @ [One x]
            else acc @ [Many (ct+1, x)]
        | x :: y :: ys ->
            if x <> y then
                if ct = 0 then aux 0 (acc @ [One x]) (y :: ys)
                else aux 0 (acc @ [Many (ct+1, x)]) (y :: ys)
            else aux (ct+1) acc (y :: ys)
    in
    aux 0 [] ls
