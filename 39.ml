let all_primes n m =
    let is_prime n = 
        let rec is_prime_aux n d =
            d * d > n || (n mod d <> 0 && is_prime_aux n (d+1))
        in
        is_prime_aux n 2
    in
    let rec aux n m acc =
        if n > m then acc
        else
            if is_prime m then aux n (m-1) (m :: acc)
            else aux n (m-1) acc
    in
    aux n m []
